#!/bin/ksh

## Copyright 2011 Vinay Kumar K <vinaykumarkhampati@gmail.com>, Kiran Danduprolu <kirandanduprolu@yahoo.com>

#########################################################################################
##    This program is free software: you can redistribute it and/or modify		#
##    it under the terms of the GNU General Public License as published by		#
##    the Free Software Foundation, either version 3 of the License, or			#
##    (at your option) any later version.						#
##											#
##    This program is distributed in the hope that it will be useful,			#
##    but WITHOUT ANY WARRANTY; without even the implied warranty of			#
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			#
##    GNU General Public License for more details.					#
##											#
##    You should have received a copy of the GNU General Public License			#
##    along with this program.  If not, see <http://www.gnu.org/licenses/>.		#
#########################################################################################


DATABASE=$1
USERID=
PASSWD=

rm /home/vinay_kumar_k/td_metadata/database.lst


##confirmation of the table names based on dbc.tables list.
table_names_validation_script_generation()
{
rm /home/vinay_kumar_k/td_metadata/table_names_validation.run
while read M_NAME;
do
TMP_TAB_LIST=${M_NAME}.tmp
echo ".os rm /home/vinay_kumar_k/td_metadata/${M_NAME}_tables.lst"
echo ".set titledashes off"
echo ".export file='/home/vinay_kumar_k/td_metadata/${M_NAME}_tables.lst"
echo "select upper(trim(databasename))||'.'||upper(trim(tablename))( title '') from dbc.tables where trim(databasename)||'.'||trim(tablename) in ("
cat ${TMP_TAB_LIST}
echo ") and tablekind in ('T','V');"
echo ".export reset"
done < /home/vinay_kumar_k/td_metadata/macro_names.lst > /home/vinay_kumar_k/td_metadata/table_names_validation.run
}

##One single file generation process, having all the DDL statements ( reduces number of logins to database)
generate_run_script_ddl()
{
while read M_NAME;
do
echo ".os rm /home/vinay_kumar_k/td_metadata/${M_NAME}.out"
echo ".export file=/home/vinay_kumar_k/td_metadata/${M_NAME}.out"
echo "show macro $M_NAME;"
echo ".export reset"
done < /home/vinay_kumar_k/td_metadata/macro_names.lst > /home/vinay_kumar_k/td_metadata/ddl_generation.run
}

## For reformating the file we need to insert new lines at many instances where pattern matches.
insert_newline()
{
FILE_NAME=$1
SUB=$2
sed -i 's/ '$SUB'/\
&/g' $FILE_NAME
}

##Single file generation process, for importing the data into the metadata table on teradata.
import_run_file_generation()
{
rm import_tables.lst
while read MACRO_NAME
do
cat ${MACRO_NAME}_tables.lst_new >> import_tables.lst
done < /home/vinay_kumar_k/td_metadata/macro_names.lst
}

## macro list from the user input table
bteq<<EOF
.logon $DATABASE/$USERID,$PASSWD
.set width 254
.os rm /home/vinay_kumar_k/td_metadata/macro_names.lst;
.export file = /home/vinay_kumar_k/td_metadata/macro_names.lst
select macro_name (title'') from tmp_work_db.macro_list;
.export reset;
.logoff
EOF

generate_run_script_ddl

## Database list generation (for join) and actual ddl definition.
bteq<<EOF
.logon $DATABASE/$USERID,$PASSWD
.set width 254
.run file = /home/vinay_kumar_k/td_metadata/ddl_generation.run
.os rm /home/vinay_kumar_k/td_metadata/database.lst;
.export file=/home/vinay_kumar_k/td_metadata/database.lst;
select upper(trim(databasename)) (title '') from dbc.tables where tablekind in ('V', 'T') and databasename not like '$%' group by 1 order by 1 asc;
.export reset
.logoff
EOF

 

if [[ `wc -l < /home/vinay_kumar_k/td_metadata/macro_names.lst` -gt 2 ]]; then
bteq<<EOF
.logon $DATABASE/$USERID,$PASSWD
.set width 254
.set separator ' '
.os rm /home/vinay_kumar_k/td_metadata/database_tablename.lst
.export file = /home/vinay_kumar_k/td_metadata/database_tablename.lst
select trim(databasename) (title ''), trim(tablename) (title'') from dbc.tables where tablekind in ('T','V') and databasename not like '$%' and tablename not like '$%' order by 1,2 asc;
.export reset
.logoff
EOF

cat /home/vinay_kumar_k/td_metadata/database_tablename.lst |tr -s ' '|sed 's/[a-z]/\U&/g' > /home/vinay_kumar_k/td_metadata/database_tablename.lst_tmp
sed -i 's/ /\./g' /home/vinay_kumar_k/td_metadata/database_tablename.lst_tmp
sort -k 1,1 /home/vinay_kumar_k/td_metadata/database_tablename.lst_tmp > /home/vinay_kumar_k/td_metadata/database_tablename.lst
fi

 

## Find the list of possible table names from the DDL.

while read MACRO_NAME;
do
TMP_TAB_LIST=${MACRO_NAME}.tmp
rm $TMP_TAB_LIST
rm ${TMP_TAB_LIST}_bkp

sed -i -e 's/[a-z]/\U&/g' -e 's/;/ & /g' /home/vinay_kumar_k/td_metadata/${MACRO_NAME}.out
cat ${MACRO_NAME}.out |sed 's/=/ & /g' |tr [[:space:]] '\n' |tr ',' '\n' |tr '(' '\n' |tr ')' '\n' |awk -F"." '{if(NF==2 && toupper($0) != tolower($0)) print toupper($0)}' > $TMP_TAB_LIST
sed -i 's/\./ /g' $TMP_TAB_LIST ## Replacing . with blank to convert the file to space separated file for join
rm ${TMP_TAB_LIST}_sort
cat ${TMP_TAB_LIST} |sort -k 1,2 > ${TMP_TAB_LIST}_sort
mv ${TMP_TAB_LIST}_sort $TMP_TAB_LIST
if [[ `wc -l < /home/vinay_kumar_k/td_metadata/macro_names.lst` -gt 2 ]]; then
sed -i 's/ /\./g' $TMP_TAB_LIST
sort -k 1,1 $TMP_TAB_LIST > ${TMP_TAB_LIST}_tmp
uniq ${TMP_TAB_LIST}_tmp > $TMP_TAB_LIST
rm ${TMP_TAB_LIST}_tmp
join -1 1 -2 1 -o 1.1 1.2 $TMP_TAB_LIST /home/vinay_kumar_k/td_metadata/database_tablename.lst > ${MACRO_NAME}_tables.lst
else
join -1 1 -2 1 -o 1.1 1.2 $TMP_TAB_LIST /home/vinay_kumar_k/td_metadata/database.lst > ${TMP_TAB_LIST}_bkp
mv ${TMP_TAB_LIST}_bkp ${TMP_TAB_LIST}
sed -i -e s/^/\'/g -e s/$/\',/g -e 's/ /\./g' -e '$s/.$//' $TMP_TAB_LIST ## generating the IN list fo DBC query.
fi

done < /home/vinay_kumar_k/td_metadata/macro_names.lst

if [[ `wc -l < /home/vinay_kumar_k/td_metadata/macro_names.lst` -lt 2 ]] then
table_names_validation_script_generation

bteq<<EOF
.logon $DATABASE/$USERID,$PASSWD
.set width 254
.run file = /home/vinay_kumar_k/td_metadata/table_names_validation.run
.logoff
EOF

fi

while read MACRO_NAME
do
REP='\/\*'
sed -i 's/'$REP'/\
&\
/g' ${MACRO_NAME}.out
REP1='\*\/'
sed -i 's/'$REP1'/\
&\
/g' ${MACRO_NAME}.out

sed -i '/'$REP'/,/'$REP1'/ d' ${MACRO_NAME}.out

### Adding more details for the output info
cat ${MACRO_NAME}.out |sed 's/--.*//g' |tr [[:space:]] ' ' | sed 's/,/ & /g' > ${MACRO_NAME}.out_new
sed -i 's/(/&\
/g' ${MACRO_NAME}.out_new
sed -i 's/)/\
&\
/g' ${MACRO_NAME}.out_new

for i in 'ON' 'AND' 'INNER JOIN' 'LEFT OUTER JOIN' 'LEFT JOIN' 'WHERE' 'GROUP BY' 'FROM' 'RIGHT JOIN' 'RIGHT OUTER JOIN' 'FULL OUTER JOIN' 'INSERT INTO' 'DELETE FROM' 'UPDATE ' ','
do
insert_newline ${MACRO_NAME}.out_new $i
done

grep -n JOIN ${MACRO_NAME}.out_new|egrep -v "INNER|LEFT|RIGHT|FULL" |cut -d":" -f1 > newline.lst
i=0
while read line_num; do
line_num_new=$(($line_num + $i));
sed -i ''$line_num_new' s/JOIN/\
&/g' ${MACRO_NAME}.out_new
i=$(($i + 1));
done <newline.lst

sed -i 's/^ //g' ${MACRO_NAME}.out_new

rm /home/vinay_kumar_k/td_metadata/${MACRO_NAME}_tables.lst_new
set -A LINE_NUM `egrep -n "^UPDATE |^\<DELETE\>|^INSERT " ${MACRO_NAME}.out_new| cut -d":" -f1`
cp ${MACRO_NAME}.out_new ${MACRO_NAME}.out_new_bkp
p=0

while [[ $p -lt ${#LINE_NUM[@]} ]];
do
if [[ $p -ne $((${#LINE_NUM[@]}-1)) ]]; then
sed -n ''${LINE_NUM[$p]}','$((${LINE_NUM[$p+1]}-1))' p'  ${MACRO_NAME}.out_new_bkp > ${MACRO_NAME}.out_new
else sed -n ''${LINE_NUM[$p]}', $ p' ${MACRO_NAME}.out_new_bkp > ${MACRO_NAME}.out_new
fi

 

p=$(($p+1));

OPERATION=`head -1 ${MACRO_NAME}.out_new|cut -d" " -f1`

 

while read tab_name ;

do

TYPE_ALL=`grep $tab_name ${MACRO_NAME}.out_new | cut -d' ' -f1|paste -s -`

 

for TYPE in $TYPE_ALL ; do

 

if [[ $TYPE == "INNER" || $TYPE == "JOIN" || $TYPE == "," ]]; then

print $MACRO_NAME "|" $OPERATION "|" $tab_name "|" "INNER JOIN"

fi

if [[ $TYPE == "LEFT" ]]; then

print $MACRO_NAME "|" $OPERATION "|" $tab_name "|" "LEFT OUTER JOIN"

fi

if [[ $TYPE == "RIGHT" ]]; then

print $MACRO_NAME "|" $OPERATION "|" $tab_name "|" "RIGHT OUTER JOIN"

fi

if [[ $TYPE == "FULL" ]]; then

print $MACRO_NAME "|" $OPERATION "|" $tab_name "|" "FULL OUTER JOIN"

fi

if [[ $TYPE == "FROM" ]]; then

print $MACRO_NAME "|" $OPERATION "|" $tab_name "|" "FROM"

fi

if [[ $TYPE == "INSERT" ]];  then

print $MACRO_NAME "|" $OPERATION "|" $tab_name "|" "INSERT"

fi

if [[ $TYPE == "UPDATE" ]];  then

print $MACRO_NAME "|" $OPERATION "|" $tab_name "|" "UPDATE"

fi

if [[ $TYPE == "DELETE" ]];  then

print $MACRO_NAME "|" $OPERATION "|" $tab_name "|" "DELETE"

fi

done|uniq

done < /home/vinay_kumar_k/td_metadata/${MACRO_NAME}_tables.lst >> /home/vinay_kumar_k/td_metadata/${MACRO_NAME}_tables.lst_new

done

done < /home/vinay_kumar_k/td_metadata/macro_names.lst

 

rm ${MACRO_NAME}.out_new_bkp

 

import_run_file_generation

 

bteq<<EOF

.set sessions 10

.logon $DATABASE/$USERID,$PASSWD

.set charset "UTF8"

.repeat *

.import vartext '|' file =/home/vinay_kumar_k/td_metadata/import_tables.lst

using

macro_name (varchar(60)),

operation (varchar(30)),

table_name (varchar(60)),

usage (varchar(30))

insert into tmp_work_db.macro_metadata values ( :macro_name,:operation,:table_name,:usage);

.logoff

EOF
